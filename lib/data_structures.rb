# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  minimum = arr.min
  maximum = arr.max

  maximum - minimum
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  str.downcase.count("aeiou")
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  str.delete("aeiouAEIOU")
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.split("").sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  letters_arr = str.downcase.split("")
  letters_arr.each_with_index do |letter, index|
    return true if letters_arr[index + 1..-1].include?(letter)
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  num_string = arr.join("")

  "(#{num_string[0..2]}) #{num_string[3..5]}-#{num_string[-4..-1]}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  str_array = str.split(",")
  num_array = []

  str_array.each do |num|
    num_array << num.to_i
  end

  num_array.sort!
  num_array.last - num_array.first
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  if offset > 0 && offset <= arr.length - 1
    last_eles = arr.take(offset)
    first_eles = arr.drop(offset)

    first_eles += last_eles
  elsif offset > 0 && offset >= arr.length - 1
    offset = offset % arr.length

    last_eles = arr.take(offset)
    first_eles = arr.drop(offset)

    first_eles += last_eles
  else
    first_eles = arr.drop(arr.length - offset.abs)
    last_eles = arr.take(arr.length - offset.abs)

    first_eles += last_eles
  end
end
